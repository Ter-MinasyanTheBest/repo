# Обработка данных с помощью R

## Цель работы

1.  Развить практические навыки использования языка программирования R
    для обработки данных
2.  Закрепить знания базовых типов данных языка R
3.  Развить пркатические навыки использования функций обработки данных
    пакета dplyr и vroom.

## Исходные данные

1.  Ноутбук HP 
2.  RStudio
3.  csv файл с данными: traffic\_security.csv.

## Задание 1: Надите утечку данных из Вашей сети

Важнейшие документы с результатми нашей исследовательской деятельности в
области создания вакцин скачиваются в виде больших заархивированных
дампов. Один из хостов в нашей сети используется для пересылки этой
информации – он пересылает гораздо больше информации на внешние ресурсы
в Интернете, чем остальные компьютеры нашей сети. Определите его
IP-адрес.

## Описание файла и данных в нем

-   Описание полей датасета: timestamp,src,dst,port,bytes
-   IP адреса внутренней сети начинаются с 12-14
-   Все остальные IP адреса относятся к внешним узлам

## Решение

### Импорт данных

    library(dplyr)

    ## 
    ## Присоединяю пакет: 'dplyr'

    ## Следующие объекты скрыты от 'package:stats':
    ## 
    ##     filter, lag

    ## Следующие объекты скрыты от 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library(vroom)

    df<-vroom("TrafficSec/traffic_security.csv", delim = ",",col_names = F)

    ## Warning in gzfile(file, mode): не могу открыть сжатый файл 'C:/Users/Pavel/
    ## AppData/Local/Temp/Rtmp0emNwl\file32cc21855a88', возможная причина -- 'No such
    ## file or directory'

    ## Rows: 105747730 Columns: 5

    ## -- Column specification --------------------------------------------------------
    ## Delimiter: ","
    ## chr (2): X2, X3
    ## dbl (3): X1, X4, X5

    ## 
    ## i Use `spec()` to retrieve the full column specification for this data.
    ## i Specify the column types or set `show_col_types = FALSE` to quiet this message.

    df %>% head(5)

    ## # A tibble: 5 x 5
    ##              X1 X2            X3              X4    X5
    ##           <dbl> <chr>         <chr>        <dbl> <dbl>
    ## 1 1578326400001 13.43.52.51   18.70.112.62    40 57354
    ## 2 1578326400005 16.79.101.100 12.48.65.39     92 11895
    ## 3 1578326400007 18.43.118.103 14.51.30.86     27   898
    ## 4 1578326400011 15.71.108.118 14.50.119.33    57  7496
    ## 5 1578326400012 14.33.30.103  15.24.31.23    115 20979

### Приводим данные к читаемому виду

    colnames(df)<-c('timestamp','src','dst','port','bytes')

    df <- df %>% mutate(rtime = lubridate::as_datetime(timestamp/1000)) %>% select(2:6)

    df %>% head(5)

    ## # A tibble: 5 x 5
    ##   src           dst           port bytes rtime              
    ##   <chr>         <chr>        <dbl> <dbl> <dttm>             
    ## 1 13.43.52.51   18.70.112.62    40 57354 2020-01-06 16:00:00
    ## 2 16.79.101.100 12.48.65.39     92 11895 2020-01-06 16:00:00
    ## 3 18.43.118.103 14.51.30.86     27   898 2020-01-06 16:00:00
    ## 4 15.71.108.118 14.50.119.33    57  7496 2020-01-06 16:00:00
    ## 5 14.33.30.103  15.24.31.23    115 20979 2020-01-06 16:00:00

### Создал функцию для определения внешнего ip

    is_src <- function(ip){
        stringr::str_detect(ip,"^1[2-4].") }

### Выполняю отбор данных, разбил на более простые операции.

    df <- df %>% mutate(is_internal = is_src(src))
    df <- df %>% mutate(is_dist = !is_src(dst))

    df<-df %>% filter(is_internal & is_dist)

    df %>% head(5)

    ## # A tibble: 5 x 7
    ##   src           dst           port bytes rtime               is_internal is_dist
    ##   <chr>         <chr>        <dbl> <dbl> <dttm>              <lgl>       <lgl>  
    ## 1 13.43.52.51   18.70.112.62    40 57354 2020-01-06 16:00:00 TRUE        TRUE   
    ## 2 14.33.30.103  15.24.31.23    115 20979 2020-01-06 16:00:00 TRUE        TRUE   
    ## 3 12.46.104.126 16.25.76.33    123  1500 2020-01-06 16:00:00 TRUE        TRUE   
    ## 4 12.43.98.93   18.85.31.68     79   979 2020-01-06 16:00:00 TRUE        TRUE   
    ## 5 13.48.126.55  18.100.109.~   123  1500 2020-01-06 16:00:00 TRUE        TRUE

## Итоговый результат

    df%>% group_by(src) %>%summarize(agress=sum(bytes)) %>% arrange(agress) %>% tail(1)

    ## # A tibble: 1 x 2
    ##   src               agress
    ##   <chr>              <dbl>
    ## 1 13.37.84.125 10625497574

## Вывод

В результате выполнения данной практической работы мы смогли обнаружить
ip адрес злоумышленника.
